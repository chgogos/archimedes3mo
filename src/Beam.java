import org.apache.log4j.Logger;
import org.moeaframework.core.Problem;
import org.moeaframework.core.Solution;
import org.moeaframework.core.Variable;
import org.moeaframework.core.variable.EncodingUtils;

public class Beam implements Problem {
	static Logger log = Logger.getLogger(Beam.class.getName());
	public static MatlabAgent ma;
	private int numberOfVariables;
	protected int numberOfObjectives;
	protected int[] objectives;
	private int numberOfConstraints;

	static int evaluation = 0;

	public static int getNumberOfActuators(Variable var) {
		boolean[] d = EncodingUtils.getBinary(var);
		int c = 0;
		for (int i = 0; i < d.length; i++) {
			if (d[i])
				c++;
		}
		return c;
	}

	public Beam() {
		this.numberOfVariables = 1;
		this.numberOfObjectives = 5;
		this.objectives = new int[] { 0, 1, 2, 3, 4 };
		this.numberOfConstraints = 1;
	}

	@Override
	public String getName() {
		return "Beam";
	}

	@Override
	public int getNumberOfVariables() {
		return numberOfVariables;
	}

	@Override
	public int getNumberOfObjectives() {
		return numberOfObjectives;
	}

	@Override
	public int getNumberOfConstraints() {
		return numberOfConstraints;
	}

	@Override
	public void evaluate(Solution solution) {
		evaluation++;
		boolean[] d = EncodingUtils.getBinary(solution.getVariable(0));

		int c = 0;
		for (int i = 0; i < d.length; i++) {
			if (d[i])
				c++;
		}
		// int holes = countHoles(d);
		// System.out.println("Holes " + holes);
		if (c >= MatlabAgent.MIN_NUMBER_OF_ACTUATORS
				&& c <= MatlabAgent.MAX_NUMBER_OF_ACTUATORS) {
			solution.setConstraint(0, 0.0); // constraints that are satisfied
											// have a value of zero
			double[] objs = ma.getObjectives(d, objectives);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < numberOfObjectives; i++) {
				if (i == 0) {
					sb.append(String.format("Eval#%d %s [nac=%d] CI=%e ",
							evaluation, solution.getVariable(0), c, objs[i]));
				} else {
					sb.append(String.format("NF%d=%.8f ", objectives[i],
							objs[i]));
				}
			}
			log.debug(sb.toString());
			solution.setObjectives(objs);
		} else if (c > MatlabAgent.MAX_NUMBER_OF_ACTUATORS) {
			solution.setConstraint(0, c - MatlabAgent.MAX_NUMBER_OF_ACTUATORS);
			double[] objs = new double[numberOfObjectives];
			solution.setObjectives(objs);
		} else if (c < MatlabAgent.MIN_NUMBER_OF_ACTUATORS) {
			solution.setConstraint(0, MatlabAgent.MIN_NUMBER_OF_ACTUATORS - c);
			double[] objs = new double[numberOfObjectives];
			solution.setObjectives(objs);
		}
		// else {
		// solution.setConstraint(0, holes);
		// double[] objs = new double[numberOfObjectives];
		// solution.setObjectives(objs);
		// }
	}

	@Override
	public Solution newSolution() {
		Solution sol = new Solution(numberOfVariables, numberOfObjectives,
				numberOfConstraints);
		sol.setVariable(0,
				EncodingUtils.newBinary(MatlabAgent.GROUPS));
		return sol;
	}

	@Override
	public void close() {
		// do nothing
	}

	// experimental
	private boolean isContinuous(boolean[] d) {
		for (int i = 0; i < d.length - 1; i++)
			if (d[i] && !d[i + 1]) {
				for (int j = i + 2; j < d.length; j++)
					if (d[j])
						return false;
			}
		return true;
	}

	private int countHoles(boolean[] d) {
		int c = 0;
		for (int i = 0; i < d.length - 1; i++)
			if (d[i] && !d[i + 1]) {
				for (int j = i + 2; j < d.length; j++)
					if (d[j]) {
						c++;
						break;
					}
			}
		return c;
	}
}
