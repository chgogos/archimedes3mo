import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.Variable;

import rcaller.RCaller;
import rcaller.RCode;

import com.google.common.base.Stopwatch;

public class UI {
	static Logger log = Logger.getLogger(UI.class.getName());
	static DateTimeFormatter fmt = DateTimeFormat
			.forPattern("yyyy_MM_dd_HH_mm");

	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);
		UI ui = new UI();
		ui.setup();
		ui.menu();
	}

	String rExePath, matlabCodePath, matlabMFile;
	int numberOfElements, minNumberOfActuators, maxNumberOfActuators,
			normalizationType, groups;

	public void setup() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("config.properties");
			prop.load(input);
			rExePath = prop.getProperty("r_exe_path").trim();
			matlabCodePath = prop.getProperty("matlab_code_path").trim();
			matlabMFile = prop.getProperty("matlab_m_file").trim();
			numberOfElements = Integer.parseInt(prop.getProperty(
					"number_of_elements").trim());
			minNumberOfActuators = Integer.parseInt(prop.getProperty(
					"min_number_of_actuators").trim());
			maxNumberOfActuators = Integer.parseInt(prop.getProperty(
					"max_number_of_actuators").trim());
			maxNumberOfActuators = Integer.parseInt(prop.getProperty(
					"max_number_of_actuators").trim());
			normalizationType = Integer.parseInt(prop.getProperty(
					"normalization_type").trim());
			groups = Integer.parseInt(prop.getProperty(
					"groups").trim());
			log.debug("r_exe_path:              " + rExePath);
			log.debug("matlab_code_path:        " + matlabCodePath);
			log.debug("matlab_m_file:           " + matlabMFile);
			log.debug("number_of_elements:      " + numberOfElements);
			log.debug("min_number_of_actuators: " + minNumberOfActuators);
			log.debug("max_number_of_actuators: " + maxNumberOfActuators);
			log.debug("normalization_type:      " + normalizationType);
			log.debug("groups:                  " + groups);
			MatlabAgent.setGlobalParametes(groups, numberOfElements,
					minNumberOfActuators, maxNumberOfActuators);
			input.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private void menu() {
		System.out
				.println("1. Optimization 5 objectives (CI, NF1, NF2, NF3, NF4)");
		System.out.println("2. Optimization 2 objectives (CI, NF1)");
		System.out.print("Enter choice: ");
		Scanner in = new Scanner(System.in);
		int choice = in.nextInt();
		if (choice == 1) {
			System.out.print("Enter algorithm (NSGAII, SPEA2, Random):");
			String algorithm = in.next();
			System.out.print("Enter max evaluations: ");
			int maxEvaluations = in.nextInt();
			optimize5Objectives(algorithm, maxEvaluations);
		} else if (choice == 2) {
			System.out.print("Enter algorithm (NSGAII, SPEA2, Random):");
			String algorithm = in.next();
			System.out.print("Enter max evaluations: ");
			int maxEvaluations = in.nextInt();
			Class objectivesClass = Beam2O_CI_NF1.class;
			String secondObjective = "NF1";
			// System.out.print("Which NF [1-4]: ");
			// int nf = in.nextInt();
			// String secondObjective = String.format("NF%d", nf);
			// if (nf == 1) {
			// objectivesClass = Beam2O_CI_NF1.class;
			// } else if (nf == 2) {
			// objectivesClass = Beam2O_CI_NF2.class;
			// log.error("Not ready!");
			// System.exit(-1);
			// } else if (nf == 3) {
			// objectivesClass = Beam2O_CI_NF3.class;
			// log.error("Not ready!");
			// System.exit(-1);
			// } else if (nf == 4) {
			// objectivesClass = Beam2O_CI_NF4.class;
			// log.error("Not ready!");
			// System.exit(-1);
			// }
			optimize2Objectives(objectivesClass, algorithm, maxEvaluations,
					secondObjective);
		}
		in.close();
	}

	private void optimize5Objectives(String algorithm, int maxEvaluations) {
		Stopwatch timer = Stopwatch.createStarted();
		Beam.ma = new MatlabAgent(matlabCodePath, matlabMFile);
		NondominatedPopulation result = new Executor()
				.withProblemClass(Beam.class).withAlgorithm(algorithm)
				.withMaxEvaluations(maxEvaluations).run();

		double[] nci = normalize(result, normalizationType);
		double values[][] = new double[result.size()][6];

		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			values[i][0] = objs[0];
			values[i][1] = nci[i];
			values[i][2] = objs[1];
			values[i][3] = objs[2];
			values[i][4] = objs[3];
			values[i][5] = objs[4];
			Variable v = solution.getVariable(0);
			String msg = String
					.format("Solution %d %s [nac=%d] CI :%e (CI Normalized Value: %.8f) NF1:%.8f NF2:%.8f NF3:%.8f NF4:%.8f",
							i + 1, solution.getVariable(0),
							Beam.getNumberOfActuators(v), values[i][0],
							values[i][1], values[i][2], values[i][3],
							values[i][4], values[i][5]);
			log.info(msg);
		}
		Beam.ma.disposeMatlabEnv();
		timer.stop();
		System.out.println("Time elapsed: " + timer.toString());

		DateTime dt = new DateTime();
		String fn_no_extension = String.format("%s_%s_%d_5Objs",
				dt.toString(fmt), algorithm, maxEvaluations);
		String exportCSVFilename = String.format("%s.csv", fn_no_extension);
		save5ObjsCSV(result, values, exportCSVFilename);

		// String exportImageFilename = String.format("%s.jpg",
		// fn_no_extension);
		// f_plot_ci_vs_all_nfs(exportCSVFilename, exportImageFilename);
	}

	private void optimize2Objectives(Class objectivesClass, String algorithm,
			int maxEvaluations, String secondObjective) {
		Stopwatch timer = Stopwatch.createStarted();
		Beam.ma = new MatlabAgent(matlabCodePath, matlabMFile);

		NondominatedPopulation result = new Executor()
				.withProblemClass(objectivesClass).withAlgorithm(algorithm)
				.withMaxEvaluations(maxEvaluations).run();

		double[] nci = normalize(result, normalizationType);
		double values[][] = new double[result.size()][3];

		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			values[i][0] = objs[0];
			values[i][1] = nci[i];
			values[i][2] = objs[1];
			Variable v = solution.getVariable(0);
			String msg = String
					.format("Solution %d %s [nac=%d] CI :%e (Normalized Value: %.8f) %s:%.8f",
							i + 1, solution.getVariable(0),
							Beam.getNumberOfActuators(v), values[i][0],
							values[i][1], secondObjective, values[i][2]);
			log.info(msg);
		}

		Beam.ma.disposeMatlabEnv();
		timer.stop();
		log.info("Time elapsed: " + timer.toString());

		DateTime dt = new DateTime();
		String fn_no_extension = String.format("%s_%s_%d_CIvs%s",
				dt.toString(fmt), algorithm, maxEvaluations, secondObjective);
		String exportCSVFilename = String.format("%s.csv", fn_no_extension);
		save2ObjsCSV(result, values, exportCSVFilename, secondObjective);

		String exportImageFilename = String.format("%s.jpg", fn_no_extension);
		// fPlotCIvsNF1(values, exportImageFilename, secondObjective);
		f_plot_ci_vs_nf1(exportCSVFilename, exportImageFilename);
	}

	private void save2ObjsCSV(NondominatedPopulation result, double[][] values,
			String exportFilename, String second_objective) {
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFilename));
			writer.write("CI, normalized_CI, " + second_objective
					+ ", locations, nac\n");
			for (int i = 0; i < result.size(); i++) {
				Solution solution = result.get(i);
				Variable v = solution.getVariable(0);
				writer.write(String.format("%e, %.8f, %.8f, %s, %d\n",
						values[i][0], values[i][1], values[i][2], v,
						Beam.getNumberOfActuators(v)));
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void save5ObjsCSV(NondominatedPopulation result, double[][] values,
			String exportFilename) {
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFilename));
			writer.write("CI, normalized_CI, NF1, NF2, NF3, NF4, locations, nac\n");
			for (int i = 0; i < result.size(); i++) {
				Solution solution = result.get(i);
				Variable v = solution.getVariable(0);
				writer.write(String.format(
						"%e, %.8f, %.8f, %.8f, %.8f, %.8f, %s, %d\n",
						values[i][0], values[i][1], values[i][2], values[i][3],
						values[i][4], values[i][5], v,
						Beam.getNumberOfActuators(v)));
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private double[] normalize(NondominatedPopulation result, int type) {
		if (type == 1) {
			return normalize1(result);
		} else if (type == 2) {
			return normalize2(result);
		} else {
			return normalize3(result);
		}
	}

	private double[] normalize1(NondominatedPopulation result) {
		double max = -Double.MAX_VALUE;
		double min = Double.MAX_VALUE;
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			objs[0] = 1.0 / objs[0];
			if (objs[0] > max)
				max = objs[0];
			if (objs[0] < min)
				min = objs[0];
		}
		double[] nci = new double[result.size()];
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			objs[0] = 1.0 / objs[0];
			nci[i] = objs[0] / max; // GF
		}
		return nci;
	}

	private double[] normalize2(NondominatedPopulation result) {
		double max = -Double.MAX_VALUE;
		double min = Double.MAX_VALUE;
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			objs[0] = 1.0 / objs[0];
			if (objs[0] > max)
				max = objs[0];
			if (objs[0] < min)
				min = objs[0];
		}
		double[] nci = new double[result.size()];
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			objs[0] = 1.0 / objs[0];
			nci[i] = (objs[0] - min) / (max - min); // CG
		}
		return nci;
	}

	private double[] normalize3(NondominatedPopulation result) {
		double[] values = new double[result.size()];
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			values[i] = 1.0 / objs[0];
		}
		// double mean = StatUtils.mean(values);
		// double std = Math.sqrt(StatUtils.variance(values));
		// double[] nci = new double[result.size()];
		// for (int i = 0; i < result.size(); i++) {
		// nci[i] = (values[i] - mean) / std;
		// }
		// return nci;
		return StatUtils.normalize(values);
	}

	@Deprecated
	private void fPlotCIvsNF1(double[][] values, String filename,
			String secondObjective) {
		RCaller caller = new RCaller();
		caller.setRscriptExecutable(rExePath + "/Rscript.exe");
		RCode code = new RCode();
		File file;
		try {
			file = code.startPlot();
			caller.setRCode(code);
			code.R_require("dplyr");
			code.addDoubleMatrix("v", values);
			code.addRCode("source('fPlotCIvsNF1.R')");
			String rcmd = String.format("fPlotCIvsNF1(v,'%s')", filename);
			code.addRCode(rcmd);
			code.endPlot();
			caller.setRCode(code);
			caller.runOnly();
			code.showPlot(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void f_plot_ci_vs_nf1(String csvfilename, String filename) {
		RCaller caller = new RCaller();
		caller.setRscriptExecutable(rExePath + "/Rscript.exe");
		RCode code = new RCode();
		File file;
		try {
			file = code.startPlot();
			caller.setRCode(code);
			code.R_require("dplyr");
			code.addRCode("source('f_plot_ci_vs_nf1.R')");
			String rcmd = String.format("f_plot_ci_vs_nf1('%s', '%s')",
					csvfilename, filename);
			code.addRCode(rcmd);
			code.endPlot();
			caller.setRCode(code);
			caller.runOnly();
			code.showPlot(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void f_plot_ci_vs_all_nfs(String csvFilename, String filename) {
		RCaller caller = new RCaller();
		caller.setRscriptExecutable(rExePath + "/Rscript.exe");
		RCode code = new RCode();
		File file;
		try {
			file = code.startPlot();
			caller.setRCode(code);
			code.R_require("dplyr");
			code.addRCode("source('f_plot_ci_vs_all_nfs.R')");
			String rcmd = String.format("f_plot_ci_vs_all_nfs('%s', '%s')",
					csvFilename, filename);
			code.addRCode(rcmd);
			code.endPlot();
			caller.setRCode(code);
			caller.runOnly();
			code.showPlot(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String readRCode(String rCodeFilename) {
		File file = new File(rCodeFilename);
		String contents = "";
		try {
			contents = new Scanner(file).useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return contents;
	}
}
