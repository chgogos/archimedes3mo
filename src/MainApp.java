import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.Variable;

import com.google.common.base.Stopwatch;

public class MainApp {

	public static void main(String[] args) {
		MatlabAgent.setGlobalParametes(5, 30, 1, 10);
		Locale.setDefault(Locale.ENGLISH);
		// DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy_MM_dd_HH_mm");
		MainApp app = new MainApp();
		System.out
				.printf("Problem setting ==> number of elements=%d number of actuators=[%d,%d]\n",
						MatlabAgent.NUMBER_OF_ELEMENTS,
						MatlabAgent.MIN_NUMBER_OF_ACTUATORS,
						MatlabAgent.MAX_NUMBER_OF_ACTUATORS);
		System.out.println("1. Test matlab function call");
		System.out.println("2. *Fig8 (Dhuri-Seshu) 5 objectives");
		System.out
				.println("3. *Fig5 (Dhuri-Seshu) %NF1 change vs Controllability Index");
		System.out
				.println("4. Fig5 (Dhuri-Seshu) %NF1 to NF4 change vs Controllability Index");
		System.out
				.println("5. CI vs selected NF using NSGAII , SPEA2 and Random");
		System.out
				.println("6. %NF1 change vs Controllability Index (java jfreechart)");
		Scanner in = new Scanner(System.in);
		int choice = in.nextInt();
		if (choice == 1)
			app.testMatlabAgent();
		else if (choice == 2) {
			DateTime dt = new DateTime();
			System.out.println("Enter algorithm (NSGAII, SPEA2, Random):");
			String algo = in.next();

			System.out.println("Enter max evaluations:");
			int maxeval = in.nextInt();
			app.setFileName(String.format("%s_%s_%d_5Objectives",
					dt.toString(fmt), algo, maxeval));
			app.test5Objectives(algo, maxeval);
		} else if (choice == 3) {
			DateTime dt = new DateTime();
			System.out.print("Enter algorithm (NSGAII, SPEA2, Random):");
			String algo = in.next();
			System.out.print("Enter max evaluations:");
			int maxeval = in.nextInt();
			System.out.print("Save to file (Y/N):");
			boolean save2File = in.next().equalsIgnoreCase("Y");
			System.out.print("Show jFreeChart (Y/N):");
			boolean showJFreeChart = in.next().equalsIgnoreCase("Y");

			Stopwatch timer = Stopwatch.createStarted();
			app.setFileName(String.format("%s_%s_%d_CIvsNF1", dt.toString(fmt),
					algo, maxeval));
			app.test2Objectives(Beam2O_CI_NF1.class, algo, maxeval,
					showJFreeChart, save2File);
			timer.stop();
			System.out.println("Total time elapsed: " + timer.toString());
		} else if (choice == 4) {
			DateTime dt = new DateTime();
			System.out.print("Enter algorithm (NSGAII, SPEA2, Random):");
			String algo = in.next();

			System.out.print("Enter max evaluations:");
			int maxeval = in.nextInt();
			Stopwatch timer = Stopwatch.createStarted();

			app.setFileName(String.format("%s_%s_%d_CIvsNF1", dt.toString(fmt),
					algo, maxeval));
			app.test2Objectives(Beam2O_CI_NF1.class, algo, maxeval, false);

			app.setFileName(String.format("%s_%s_%d_CIvsNF2", dt.toString(fmt),
					algo, maxeval));
			app.test2Objectives(Beam2O_CI_NF2.class, algo, maxeval, false);

			app.setFileName(String.format("%s_%s_%d_CIvsNF3", dt.toString(fmt),
					algo, maxeval));
			app.test2Objectives(Beam2O_CI_NF3.class, algo, maxeval, false);

			app.setFileName(String.format("%s_%s_%d_CIvsNF4", dt.toString(fmt),
					algo, maxeval));
			app.test2Objectives(Beam2O_CI_NF4.class, algo, maxeval, false);

			timer.stop();
			System.out.println("Total time elapsed: " + timer.toString());
		} else if (choice == 5) {
			DateTime dt = new DateTime();
			System.out.println("Enter max evaluations:");
			int maxeval = in.nextInt();
			app.setFileName(String.format("%s_%d_NSGAII_Experiment1_CIvsNF1",
					dt.toString(fmt), maxeval));
			app.test2Objectives(Beam2O_CI_NF1.class, "NSGAII", maxeval, false);
			app.setFileName(String.format("%s_%d_SPEA2_Experiment1_CIvsNF1",
					dt.toString(fmt), maxeval));
			app.test2Objectives(Beam2O_CI_NF1.class, "SPEA2", maxeval, false);
			app.setFileName(String.format("%s_%d_Random_Experiment1_CIvsNF1",
					dt.toString(fmt), maxeval));
			app.test2Objectives(Beam2O_CI_NF1.class, "Random", maxeval, false);
		} else if (choice == 6) {
			DateTime dt = new DateTime();
			System.out.println("Enter algorithm (NSGAII, SPEA2, Random):");
			String algo = in.next();

			System.out.println("Enter max evaluations:");
			int maxeval = in.nextInt();
			Stopwatch timer = Stopwatch.createStarted();

			app.setFileName(String.format("%s_%s_%d_CIvsNF1", dt.toString(fmt),
					algo, maxeval));
			app.test2Objectives(Beam2O_CI_NF1.class, algo, maxeval, true);
			timer.stop();
			System.out.println("Total time elapsed: " + timer.toString());
		}
		in.close();
	}

	private String fileName = "default.txt";
	String matlabCodePath = "C:/Users/chgogos/Dropbox/projects/MULTI_3DoF"; 
	String matlabMFile = "test_gogos_gf";		

	private void setFileName(String fn) {
		this.fileName = fn;

	}

	public void testMatlabAgent() {
		Stopwatch timer = Stopwatch.createStarted();
		MatlabAgent ma = new MatlabAgent(matlabCodePath, matlabMFile);
		
		boolean[] dummy = new boolean[MatlabAgent.NUMBER_OF_ELEMENTS];

		for (int i = 0; i < MatlabAgent.MAX_NUMBER_OF_ACTUATORS; i++)
			dummy[29 - i] = true;

		// dummy[27]=true;
		// dummy[28]=true;
		// dummy[29]=true;

		double[] r = ma.getObjectives(dummy, new int[] { 0, 1, 2, 3, 4 });
		System.out.printf("obj1=%e obj2=%.8f obj3=%.8f obj4=%.8f obj5=%.8f\n",
				r[0], r[1], r[2], r[3], r[4]);
		ma.disposeMatlabEnv();
		timer.stop();
		System.out.println("Time elapsed: " + timer.toString());
	}

	public void test5Objectives(String algo, int maxeval) {
		Stopwatch timer = Stopwatch.createStarted();
		Beam.ma = new MatlabAgent(matlabCodePath, matlabMFile);
		// NondominatedPopulation result = new Executor()
		// .withProblemClass(Beam.class).withAlgorithm(algo)
		// .withMaxEvaluations(maxeval).distributeOnAllCores().run();
		NondominatedPopulation result = new Executor()
				.withProblemClass(Beam.class).withAlgorithm(algo)
				.withMaxEvaluations(maxeval).run();

		double max = -Double.MAX_VALUE;
		double min = Double.MAX_VALUE;
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			objs[0] = 1.0 / objs[0];
			if (objs[0] > max)
				max = objs[0];
			if (objs[0] < min)
				min = objs[0];
		}

		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(fileName));
			writer.write("CI, normalized_CI, NF1, NF2, NF3, NF4, locations, nac\n");
			for (int i = 0; i < result.size(); i++) {
				Solution solution = result.get(i);
				Variable v = solution.getVariable(0);
				double[] objs = solution.getObjectives();
				objs[0] = 1.0 / objs[0];
				System.out.printf("Solution %d %s\t", i + 1, v);
				// double normalized_value_obj0 = (objs[0] - min) / (max - min);
				double normalized_value_obj0 = objs[0] / max;
				System.out.printf("CI:%e nac=%d", objs[0],
						Beam.getNumberOfActuators(v));
				System.out.printf("normalized_CI:%.8f ", normalized_value_obj0);
				System.out.printf("NF1:%.8f ", objs[1]);
				System.out.printf("NF2:%.8f ", objs[2]);
				System.out.printf("NF3:%.8f ", objs[3]);
				System.out.printf("NF4:%.8f\n", objs[4]);
				writer.write(String.format(
						"%e, %.8f, %.8f, %.8f, %.8f, %.8f, \"%s\", %d\n",
						objs[0], normalized_value_obj0, objs[1], objs[2],
						objs[3], objs[4], v, Beam.getNumberOfActuators(v)));
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Beam.ma.disposeMatlabEnv();
		timer.stop();
		System.out.println("Time elapsed: " + timer.toString());
	}

	public void test2Objectives(Class class1, String algorithm,
			int maxEvaluations, boolean showGraph) {
		test2Objectives(class1, algorithm, maxEvaluations, showGraph, true);
	}

	// algorithm can be NSGAII, SPEA2, Random
	public void test2Objectives(Class class1, String algorithm,
			int maxEvaluations, boolean showGraph, boolean saveFile) {
		Stopwatch timer = Stopwatch.createStarted();
		Beam.ma = new MatlabAgent(matlabCodePath, matlabMFile);

		String second_objective = "undefined";
		if (class1 == Beam2O_CI_NF1.class) {
			second_objective = "NF1";
			// Beam.ma.setType("CIvsNF1");
		} else if (class1 == Beam2O_CI_NF2.class) {
			second_objective = "NF2";
			// Beam.ma.setType("CIvsNF2");
		} else if (class1 == Beam2O_CI_NF3.class) {
			second_objective = "NF3";
			// Beam.ma.setType("CIvsNF3");
		} else if (class1 == Beam2O_CI_NF4.class) {
			second_objective = "NF4";
			// Beam.ma.setType("CIvsNF4");
		}

		NondominatedPopulation result = new Executor().withProblemClass(class1)
				.withAlgorithm(algorithm).withMaxEvaluations(maxEvaluations)
				.run();
		// NondominatedPopulation result = new
		// Executor().withProblemClass(class1)
		// .withAlgorithm(algorithm).withMaxEvaluations(maxEvaluations)
		// .distributeOnAllCores().run();

		double max = -Double.MAX_VALUE;
		double min = Double.MAX_VALUE;
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			objs[0] = 1.0 / objs[0];
			if (objs[0] > max)
				max = objs[0];
			if (objs[0] < min)
				min = objs[0];
		}
		double values[][] = new double[result.size()][3];
		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			double[] objs = solution.getObjectives();
			objs[0] = 1.0 / objs[0];
			double normalized_value_obj0 = objs[0] / max;
			// double normalized_value_obj0 = (objs[0] - min) / (max - min);
			values[i][0] = objs[0];
			values[i][1] = normalized_value_obj0;
			values[i][2] = objs[1];
		}

		for (int i = 0; i < result.size(); i++) {
			Solution solution = result.get(i);
			Variable v = solution.getVariable(0);
			System.out
					.printf("Solution %d %s [nac=%d] CI :%e (Normalized Value: %.8f) %s:%.8f\n",
							i + 1, solution.getVariable(0),
							Beam.getNumberOfActuators(v), values[i][0],
							values[i][1], second_objective, values[i][2]);
		}

		if (saveFile) {
			BufferedWriter writer;
			try {
				writer = new BufferedWriter(new FileWriter(fileName));
				writer.write("CI, normalized_CI, " + second_objective
						+ ", locations, nac\n");
				for (int i = 0; i < result.size(); i++) {
					Solution solution = result.get(i);
					Variable v = solution.getVariable(0);
					writer.write(String.format("%e, %.8f, %.8f, %s, %d\n",
							values[i][0], values[i][1], values[i][2], v,
							Beam.getNumberOfActuators(v)));
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (showGraph) {
			showGraph(values);
		}
		Beam.ma.disposeMatlabEnv();
		timer.stop();
		System.out.println("Time elapsed: " + timer.toString());
	}

	private static void showGraph(double[][] values) {
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries data = new XYSeries("CIvsNF1");
		for (int i = 0; i < values.length; i++) {
			double x0 = values[i][2]; // NF1
			double x1 = values[i][1]; // CI_normalized
			data.add(x0, x1);
		}
		dataset.addSeries(data);
		final JFreeChart chart = createChart(dataset);
		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		final ApplicationFrame frame = new ApplicationFrame("Title");
		frame.setContentPane(chartPanel);
		frame.pack();
		frame.setVisible(true);
	}

	private static JFreeChart createChart(final XYDataset dataset) {
		final JFreeChart chart = ChartFactory.createScatterPlot(
				"CIvsNF1 chart", // title
				"Natural Frequency 1", // x axis label
				"Controlability Index", // y axis label
				dataset, // data
				PlotOrientation.VERTICAL, true, // include legend
				true, // tooltips
				false // urls
				);
		XYPlot plot = (XYPlot) chart.getPlot();
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setSeriesLinesVisible(0, true);
		plot.setRenderer(renderer);
		return chart;
	}

}
