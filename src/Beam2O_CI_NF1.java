public class Beam2O_CI_NF1 extends Beam {

	public Beam2O_CI_NF1() {
		super();
		this.numberOfObjectives = 2;
		this.objectives = new int[] { 0, 1 };
	}
}
