import java.util.Random;

import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.MatlabProxyFactoryOptions;

public class MatlabAgent {

	Random r;
	MatlabProxy proxy;
	static int NUMBER_OF_ELEMENTS = -1;
	static int MIN_NUMBER_OF_ACTUATORS = -1;
	static int MAX_NUMBER_OF_ACTUATORS = -1;
	static int GROUPS = -1;

	public static void setGlobalParametes(int groups, int numberOfElements,
			int minNumberOfActuators, int maxNumberOfActuators) {
		NUMBER_OF_ELEMENTS = numberOfElements;
		MIN_NUMBER_OF_ACTUATORS = minNumberOfActuators;
		MAX_NUMBER_OF_ACTUATORS = maxNumberOfActuators;
		GROUPS = groups;
	}

	String matlabCodePath;
	String matlabMFile;

	public MatlabAgent(String matlabCodePath, String matlabMFile) {
		r = new Random(123456789L);
		this.matlabCodePath = matlabCodePath;
		this.matlabMFile = matlabMFile;
		MatlabProxyFactoryOptions options = new MatlabProxyFactoryOptions.Builder()
				.setUsePreviouslyControlledSession(true).setHidden(true)
				.setMatlabLocation(null).build();
		MatlabProxyFactory factory = new MatlabProxyFactory(options);
		try {
			proxy = factory.getProxy();
			proxy.eval("addpath('" + matlabCodePath + "')");
		} catch (MatlabConnectionException e) {
			e.printStackTrace();
		} catch (MatlabInvocationException e) {
			e.printStackTrace();
		}
	}

	public double[] getObjectives(boolean[] a, int[] obj_ids) {
		int numberOfObjectives = 0;
		for (int i = 0; i < obj_ids.length; i++) {
			if (obj_ids[i] > numberOfObjectives)
				numberOfObjectives = obj_ids[i];
		}
		numberOfObjectives++;
		double[] rv = new double[obj_ids.length];
		try {
			proxy.eval("addpath('" + matlabCodePath + "')");
			Object[] args = new Object[2];
			int[] alpha = new int[GROUPS];
			int c = 0;
			for (int i = 0; i < a.length; i++) {
				alpha[i] = a[i] ? 1 : 0;
				c += alpha[i];
			}
			args[0] = alpha;
			args[1] = c;
			Object[] ro;

			ro = proxy.returningFeval(matlabMFile, 5, args);
			int i = 0;
			for (int ri : obj_ids) {
				rv[i] = ((double[]) ro[ri])[0];
				i++;
			}
		} catch (MatlabInvocationException e) {
			e.printStackTrace();
		}
		return rv;
	}

	public void disposeMatlabEnv() {
		try {
			proxy.eval("rmpath('" + matlabCodePath + "')");
			// close connection
			proxy.disconnect();
			// proxy.exit();
		} catch (MatlabInvocationException e) {
			e.printStackTrace();
		}

	}

	public double[] getRandomGeneratedObjectives(boolean[] a) {
		double[] objectives = new double[5];
		objectives[0] = r.nextDouble() * 100;
		objectives[1] = r.nextDouble() * 200;
		objectives[2] = r.nextDouble() * 300;
		objectives[3] = r.nextDouble() * 400;
		objectives[4] = r.nextDouble() * 500;
		return objectives;
	}

}
